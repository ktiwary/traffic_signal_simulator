# README #

The project uses JDK 8 and Scala SDK 2.11.7

### What is this repository for? ###

* A simulation of traffic lights intersection using Akka's inbuilt FSM implementation
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Import the project as a Scala SBT project
* Needs akka-actor as a dependency
* Run TrafficFSMActorTest

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact