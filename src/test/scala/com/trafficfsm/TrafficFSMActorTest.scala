package com.trafficfsm

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.actor.FSM.{CurrentState, SubscribeTransitionCallBack, Transition}
import akka.testkit.{ImplicitSender, TestFSMRef, TestKit}
import org.scalatest._
import scala.concurrent.duration.{FiniteDuration, _}

/**
 * Created by kunaltiwary on 10/18/15.
 */
class TrafficFSMActorTest(as: ActorSystem) extends TestKit(as)
with ImplicitSender
with WordSpecLike
with Matchers
with BeforeAndAfterAll {

  implicit val timeout = FiniteDuration(5, TimeUnit.SECONDS)

  val signalData = Data(timeout)


  def this() = this(ActorSystem("testActorSystem"))

  val fsmRef = TestFSMRef(new TrafficFSMActor)
  fsmRef ! SubscribeTransitionCallBack(fsmRef)

  "TrafficSignal FSM Actor" must {
    "be at State0 at initial stage " in {
      assert(fsmRef.stateName == State0)
      assert(fsmRef.stateName.fromDirection == NSGreen)
      assert(fsmRef.stateName.toDirection == EWRed)
    }

    "change to State1 after ChangeSignal event" in {

      fsmRef ! SubscribeTransitionCallBack(testActor)

      expectMsgPF(5.second, "Validating current state as State0") {
        case CurrentState(`fsmRef`, State0) => print("Valid Current state")
      }

      fsmRef ! ChangeSignal

      within(0.second, 5.second) {
        expectMsg(Transition(fsmRef, State0, State1))
      }

      assert(fsmRef.stateName == State1)
      assert(fsmRef.stateName.fromDirection == NSYellow)
    }

    "change to State2 after ChangeSignal event" in {
      fsmRef ! SubscribeTransitionCallBack(testActor)

      expectMsgPF(5.second, "Validating current state as State1") {
        case CurrentState(`fsmRef`, State1) => print("Valid Current state")
      }

      fsmRef ! ChangeSignal

      within(0.second, 5.second) {
        expectMsg(Transition(fsmRef, State1, State2))
      }
      assert(fsmRef.stateName == State2)
      assert(fsmRef.stateName.fromDirection == NSRed)
    }

    "change to State3 after ChangeSignal event" in {
      fsmRef ! SubscribeTransitionCallBack(testActor)

      expectMsgPF(5.second, "Validating current state as State3") {
        case CurrentState(`fsmRef`, State2) => print("Valid Current state")
      }

      fsmRef ! ChangeSignal

      within(0.second, 5.second) {
        expectMsg(Transition(fsmRef, State2, State3))
      }
      assert(fsmRef.stateName == State3)
      assert(fsmRef.stateName.fromDirection == NSRed)
    }

    "change to State4 after ChangeSignal event" in {
      fsmRef ! SubscribeTransitionCallBack(testActor)

      expectMsgPF(5.second, "Validating current state as State04") {
        case CurrentState(`fsmRef`, State3) => print("Valid Current state")
      }

      fsmRef ! ChangeSignal

      within(0.second, 5.second) {
        expectMsg(Transition(fsmRef, State3, State4))
      }
      assert(fsmRef.stateName == State4)
      assert(fsmRef.stateName.fromDirection == NSRed)
    }

    "change to State5 after ChangeSignal event" in {
      fsmRef ! SubscribeTransitionCallBack(testActor)

      expectMsgPF(5.second, "Validating current state as State5") {
        case CurrentState(`fsmRef`, State4) => print("Valid Current state")
      }

      fsmRef ! ChangeSignal

      within(0.second, 5.second) {
        expectMsg(Transition(fsmRef, State4, State5))
      }

      assert(fsmRef.stateName == State5)
      assert(fsmRef.stateName.fromDirection == NSRed)
    }

  }

  override protected def afterAll(): Unit = {
    println("Traffic signals reset")
  }
}
