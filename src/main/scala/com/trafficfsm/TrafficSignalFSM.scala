package com.trafficfsm

import java.util.concurrent.TimeUnit

import akka.actor.FSM.{CurrentState, Transition}
import akka.actor.{Actor, FSM, LoggingFSM}

import scala.concurrent.duration.FiniteDuration

/**
 * Created by kunaltiwary on 10/18/15.
 */

//States(Direction and Color combination) that the signal can be in
sealed trait DirectionColor

case object NSGreen extends DirectionColor

case object NSYellow extends DirectionColor

case object NSRed extends DirectionColor

case object EWGreen extends DirectionColor

case object EWRed extends DirectionColor

case object EWYellow extends DirectionColor

sealed trait FSMSignalState {
  def fromDirection: DirectionColor

  def toDirection: DirectionColor
}

case object State0 extends FSMSignalState {
  override def fromDirection = NSGreen

  override def toDirection = EWRed
}

case object State1 extends FSMSignalState {
  override def fromDirection = NSYellow

  override def toDirection = EWGreen
}

case object State2 extends FSMSignalState {
  override def fromDirection = NSRed

  override def toDirection = EWRed
}

case object State3 extends FSMSignalState {
  override def fromDirection = NSRed

  override def toDirection = NSGreen
}

case object State4 extends FSMSignalState {
  override def fromDirection = NSRed

  override def toDirection = NSYellow
}

case object State5 extends FSMSignalState {
  override def fromDirection = NSRed

  override def toDirection = EWRed
}


// Data is used while reacting to events
case class Data(start: FiniteDuration)

// Events to which actor reacts
// Pedestrian button press etc can be added
case object ChangeSignal


class TrafficFSMActor extends Actor with LoggingFSM[FSMSignalState, Data] {

  implicit val timeout = FiniteDuration(5, TimeUnit.SECONDS)
  implicit val yellowTimeout = FiniteDuration(30, TimeUnit.SECONDS)

  // Initial state of FSM
  startWith(State0, Data(timeout))


  when(State0) {
    case Event(ChangeSignal, Data(_)) => goto(State1) forMax timeout
  }

  when(State1, timeout) {
    case Event(ChangeSignal, _) => goto(State2) forMax timeout
  }

  when(State2) {
    case Event(ChangeSignal, _) => goto(State3) forMax timeout
  }

  when(State3) {
    case Event(ChangeSignal, _) => goto(State4) forMax timeout
  }

  when(State4) {
    case Event(ChangeSignal, _) => goto(State5) forMax timeout
  }

  when(State5) {
    case Event(ChangeSignal, _) => goto(State0) forMax timeout
  }

  onTransition {
    case State0 -> State1 =>
      log info "Changing from (NSGreen,EWRed) to (NSYellow,EWRed)"
    case State1 -> State2 =>
      log info "Changing from (NSYellow,EWRed) to (NSRed,EWRed)"
    case State2 -> State3 =>
      log info "Changing from (NSRed,EWRed) to (NSRed,EWGreen)"
    case State3 -> State4 =>
      log info "Changing from (NSRed,EWGreen) to (NSRed,EWYellow)"
    case State4 -> State5 =>
      log info "Changing from (NSRed,EWYellow) to (NSRed,EWRed)"
  }

  whenUnhandled {
    case Event(Transition(_, from, to), _) => stay()
    case Event(CurrentState, _) => stay()
    case Event(StateTimeout, _) => stay()
    case Event(CurrentState, _) => stay()
  }

  onTermination {
    case StopEvent(FSM.Failure(_), state, data) ⇒
      val lastEvents = getLog.mkString("\n\t")
      log.warning("Failure in state " + state + " with data " + data + "\n" +
        "Events leading up to this point:\n\t" + lastEvents)
  }

  initialize()
}