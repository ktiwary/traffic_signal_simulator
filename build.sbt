name := "traffic_signal_simulator"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "ch.qos.logback" % "logback-classic" % "1.1.2",
  "com.typesafe.akka" %% "akka-actor" % "2.4.0",
  "org.scalatest" % "scalatest_2.11" % "2.2.5" % "test",
  "com.typesafe.akka" % "akka-testkit_2.11" % "2.4.0" % "test")